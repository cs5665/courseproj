# -*- coding: utf-8 -*-

https://colab.research.google.com/drive/1ejaxFohPXroPiwGHG4IePZuYq-RfaK-O


# Commented out IPython magic to ensure Python compatibility.
# %matplotlib inline
import numpy as np
import pandas as pd

train_data = pd.read_csv('new_train_data.csv')
test_data = pd.read_csv('new_test_data.csv')

all_features = pd.concat((train_data.iloc[:, 1:-1], test_data.iloc[:, 1:]))

numerical_features= all_features.columns[all_features.dtypes!='object']
numerical_features_mean=all_features.loc[:,numerical_features].mean()
numerical_features_std=all_features.loc[:,numerical_features].std()
all_features.loc[:, numerical_features]=(all_features.loc[:,numerical_features]-numerical_features_mean)/numerical_features_std
all_features = all_features.fillna(0)
all_features=pd.get_dummies(all_features, dummy_na = True)
n_train = train_data.shape[0]
train_features = all_features[:n_train].values
test_features = all_features[n_train:].values
train_data
train_labels = train_data.time_to_eruption.values.reshape((-1, 1))

from sklearn.linear_model import Ridge

from sklearn.linear_model import RidgeCV
ridgecv = RidgeCV(alphas=[0.01, 0.1, 0.5, 1, 5, 10, 15, 20, 50, 100])
ridgecv.fit(train_features, train_labels)
ridgecv.alpha_

model = Ridge(alpha=15)
model.fit(train_features,train_labels)
model.score(train_features, train_labels)

def train_and_pred(test_feature, test_data,):
    preds = model.predict(test_features)
    # reformat it for export to Kaggle
    test_data['time_to_eruption'] = pd.Series(preds.reshape(1, -1)[0])
    submission = pd.concat([test_data['segment_id'], test_data['time_to_eruption']], axis=1)
    submission.to_csv('submission.csv', index=False)

train_and_pred(test_features, test_data)
